﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace RawDiskCopy
{
    class Program
    {
        public class Drive
        {
            public string Name { get; set; }
            public int BytesPerSector { get; set; }
            public int TotalSectors { get; set; }
            public int SectorsPerTrack { get; set; }
            public int TotalTracks { get; set; }
            public long TotalBytes
            {
                get
                {
                    return (long)BytesPerSector * (long)TotalSectors;
                }
            }
        }
        public static List<Drive> GetDriveList()
        {
            List<Drive> drivelist = new List<Drive>();
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_DiskDrive");
            foreach (ManagementObject queryObj in searcher.Get())
            {
                try
                {
                    drivelist.Add(new Drive()
                    {
                        Name = queryObj["DeviceID"].ToString(),
                        BytesPerSector = int.Parse(queryObj["BytesPerSector"].ToString()),
                        TotalSectors = int.Parse(queryObj["TotalSectors"].ToString()),
                        SectorsPerTrack = int.Parse(queryObj["SectorsPerTrack"].ToString()),
                        TotalTracks = int.Parse(queryObj["TotalTracks"].ToString())
                    });
                }
                catch
                {

                }
            }

            return drivelist;
        }

        #region "API CALLS"

        public enum EMoveMethod : uint
        {
            Begin = 0,
            Current = 1,
            End = 2
        }

        [DllImport("Kernel32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern uint SetFilePointer(
            [In] SafeFileHandle hFile,
            [In] int lDistanceToMove,
            [Out] out int lpDistanceToMoveHigh,
            [In] EMoveMethod dwMoveMethod);

        [DllImport("kernel32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        static extern SafeFileHandle CreateFile(string lpFileName, uint dwDesiredAccess,
          uint dwShareMode, IntPtr lpSecurityAttributes, uint dwCreationDisposition,
          uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        [DllImport("kernel32", SetLastError = true)]
        internal extern static int ReadFile(SafeFileHandle handle, byte[] bytes,
           int numBytesToRead, out int numBytesRead, IntPtr overlapped_MustBeZero);

        #endregion

        public static SafeFileHandle CreateHandle(string drive)
        {
            short FILE_ATTRIBUTE_NORMAL = 0x80;
            short INVALID_HANDLE_VALUE = -1;
            uint GENERIC_READ = 0x80000000;
            uint GENERIC_WRITE = 0x40000000;
            uint CREATE_NEW = 1;
            uint CREATE_ALWAYS = 2;
            uint OPEN_EXISTING = 3;
            SafeFileHandle handleValue = CreateFile(drive, GENERIC_READ, 0, IntPtr.Zero, OPEN_EXISTING, 0, IntPtr.Zero);
            if (handleValue.IsInvalid)
            {
                Marshal.ThrowExceptionForHR(Marshal.GetHRForLastWin32Error());
            }
            return handleValue;
        }

        private static byte[] DumpBytes(SafeFileHandle handle, long offset, int size, out int read)
        {
            byte[] buf = new byte[size];
            Seek(handle, offset, EMoveMethod.Begin);
            ReadFile(handle, buf, size, out read, IntPtr.Zero);
            if (read == 0)
                throw new Exception("Couldn't read anything");
            return buf;
        }

        public static long Seek(SafeFileHandle handle, long offset, EMoveMethod origin)
        {
            int lo = (int)(offset & 0xffffffff);
            int hi = (int)(offset >> 32);
            lo = (int)SetFilePointer(handle, lo, out hi, origin);
            if (lo == -1)
            {
                throw new Exception("INVALID_SET_FILE_POINTER");
            }
            return (((long)hi << 32) | (uint)lo);
        }
        static void Main(string[] args)
        {
            Console.Write("FilePath: ");
            string filePath = Console.ReadLine();
            FileStream fileStream = new FileStream(filePath, FileMode.Create);
            long currentByteOffset = 0;
            int readSize = 520192;
            int bytePerSector = 512;
            int currentPercent = 0;
            int percentDistance = 10000;
            List<long> badSectors = new List<long>();
            DateTime prevTime = DateTime.Now;
            SafeFileHandle handle = null;
            while (true)
            {
                Console.Clear();
                try
                {
                    Console.WriteLine("Current Byte Offset: " + currentByteOffset);
                    Console.WriteLine("Disks: ");
                    List<Drive> drives = GetDriveList();
                    for (int i = 0; i < drives.Count; i++)
                        Console.WriteLine(i + "-" + drives[i].Name + '-' + drives[i].TotalBytes);
                    Console.WriteLine("-1 Refresh");
                    Console.Write("Choice: ");
                    int choice = int.Parse(Console.ReadLine());
                    if (choice == -1)
                    {
                        continue;
                    }
                    Drive selectedDevice = drives[choice];
                    handle = CreateHandle(selectedDevice.Name);
                    int read = 0;
                    DumpBytes(handle, 0, readSize, out read);
                    while (currentByteOffset < selectedDevice.TotalBytes)
                    {
                        try
                        {
                            if(currentByteOffset== 3162247168)
                            {
                                currentByteOffset += 4096;
                                fileStream.Write(new byte[4096], 0, 4096);
                                continue;
                            }
                            //Console.Clear();
                            byte[] readData = DumpBytes(handle, currentByteOffset, readSize, out read);
                            if (readSize != read)
                            {
                                Console.Write("Wierd");
                            }
                            fileStream.Write(readData, 0, read);
                            currentByteOffset += read;
                            int percent = (int)(((double)currentByteOffset / drives[choice].TotalBytes) * percentDistance);
                            if (percent > currentPercent)
                            {
                                fileStream.Flush();
                                Console.Clear();
                                currentPercent = percent;
                                TimeSpan diff = DateTime.Now - prevTime;
                                prevTime = DateTime.Now;
                                int doneSectors = drives[choice].TotalSectors / percentDistance;
                                int doneBytes = doneSectors * drives[choice].BytesPerSector;
                                double speed = (double)doneBytes / diff.TotalMilliseconds * 1000 / 1024 / 1024;
                                Console.WriteLine("Done: " + (float)percent / (percentDistance / 100) + "% Speed: " + speed.ToString("0.00") + "B/S");
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.Beep();
                            currentByteOffset += 512;
                            fileStream.Write(new byte[512], 0, 512);
                            Console.WriteLine("Bad Sector at: " + currentByteOffset / 512);
                            badSectors.Add(currentByteOffset / 512);
                            throw new Exception(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    handle.Close();
                    Console.WriteLine("Error: " + ex.Message);
                    Console.WriteLine("Press Any Key...");
                    Console.ReadKey();
                }
            }



        }
    }
}
